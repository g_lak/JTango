//+======================================================================
// $Source$
//
// Project:   Tango
//
// Description:  java source code for the TANGO client/server API.
//
// $Author: pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Revision: 25296 $
//
//-======================================================================


package fr.esrf.TangoApi;


import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;

import static org.tango.utils.DevFailedUtils.buildDevError;


public class CommonDevFailed extends DevFailed implements java.io.Serializable {
    public CommonDevFailed(DevError[] errors) {
        super(errors);
    }

    public CommonDevFailed(DevFailed df, String reason, String desc, String origin, ErrSeverity severity) {
        super(reason, buildDevError(df, reason, desc, origin, severity));
        if (df != null) this.initCause(df);
    }

    public CommonDevFailed(DevFailed df, String reason, String desc, ErrSeverity severity, int stackLevel) {
        super(reason, buildDevError(df, reason, desc, severity, stackLevel));
        if (df != null) this.initCause(df);
    }

    public CommonDevFailed(String reason, String desc, String origin) {
        this(null, reason, desc, origin, ErrSeverity.ERR);
    }

    public CommonDevFailed(String reason, String desc) {
        this(null, reason, desc, ErrSeverity.ERR, 4);
    }

    public CommonDevFailed(DevFailed df, String reason, String desc) {
        this(df, reason, desc, ErrSeverity.ERR, 4);
    }

    //===================================================================
    public CommonDevFailed(Throwable ex, String reason, String desc, ErrSeverity severity, int stackLevel) {
        super(reason, buildDevError(ex, reason, desc, severity, stackLevel));
        this.initCause(ex);
    }

    public CommonDevFailed(Throwable ex, String reason, String desc, String origin) {
        super(reason, buildDevError(ex, reason, desc, origin));
        this.initCause(ex);
    }

    public CommonDevFailed(Throwable ex, String reason, String desc, ErrSeverity severity) {
        this(ex, reason, desc, severity, 4);
    }

    public CommonDevFailed(Throwable ex, String reason, String desc) {
        this(ex, reason, desc, ErrSeverity.ERR, 4);
    }

    //===================================================================
    /**
     * Return exception name.
     */
    //===================================================================
    @Override
    public String toString() {
        return this.getClass().getName();
    }

    //===================================================================
    /**
     * Return full exception.
     */
    //===================================================================
    public String getStack() {
        StringBuilder sb = new StringBuilder(this + ":\n");
        for (int i = 0; i < errors.length; i++) {
            sb.append("Severity -> ");
            switch (errors[i].severity.value()) {
                case ErrSeverity._WARN:
                    sb.append("WARNING \n");
                    break;

                case ErrSeverity._ERR:
                    sb.append("ERROR \n");
                    break;

                case ErrSeverity._PANIC:
                    sb.append("PANIC \n");
                    break;

                default:
                    sb.append("Unknown severity code");
                    break;
            }
            sb.append("Desc     -> ").append(errors[i].desc).append("\n");
            sb.append("Reason   -> ").append(errors[i].reason).append("\n");
            sb.append("Origin   -> ").append(errors[i].origin).append("\n");

            if (i < errors.length - 1)
                sb.append("-------------------------------------------------------------\n");
        }
        return sb.toString();
    }
}
//-----------------------------------------------------------------------------
/* end of $Source$ */
