package org.tango.it;

import java.util.Objects;

public class TestDevice {

    private final String devName;
    private final Class clazz;
    private final String servName;
    private final String instanceName;

    public TestDevice(String devName, Class clazz, String servName, String instanceName) {
        Objects.requireNonNull(devName);
        Objects.requireNonNull(clazz);
        Objects.requireNonNull(servName);
        Objects.requireNonNull(instanceName);
        
        this.devName = devName;
        this.clazz = clazz;
        this.servName = servName;
        this.instanceName = instanceName;
    }

    public String getDevName() {
        return devName;
    }

    public Class getDevClass() {
        return clazz;
    }

    public String getServName() {
        return servName;
    }
    
    public String getFullServerName() {
        return servName + "/" + instanceName;
    }

    public String getInstanceName() {
        return instanceName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestDevice that = (TestDevice) o;

        if (!Objects.equals(devName, that.devName)) return false;
        if (!Objects.equals(clazz, that.clazz)) return false;
        if (!Objects.equals(servName, that.servName)) return false;
        return Objects.equals(instanceName, that.instanceName);
    }

    @Override
    public int hashCode() {
        int result = devName.hashCode();
        result = 31 * result + clazz.hashCode();
        result = 31 * result + servName.hashCode();
        result = 31 * result + instanceName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "TestDevice{" +
                "devName='" + devName + '\'' +
                ", clazz=" + clazz +
                ", servName='" + servName + '\'' +
                ", instanceName='" + instanceName + '\'' +
                '}';
    }
}
