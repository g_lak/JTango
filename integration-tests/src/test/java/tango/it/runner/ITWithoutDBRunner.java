package tango.it.runner;

import fr.esrf.Tango.DevFailed;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.tango.it.ITWithoutTangoDB;
import org.tango.it.TestDevice;
import org.tango.it.manager.NoDBDeviceManager;
import org.tango.server.testserver.JTangoTest;

import java.io.IOException;
import java.util.List;

import static org.tango.server.testserver.JTangoTest.DEFAULT_NO_DB_DEVICE_NAME;

public class ITWithoutDBRunner {

    private static ITWithoutTangoDB manager;

    private static void init() {
        if (manager == null) {
            manager = new NoDBDeviceManager();
        } else {
            System.out.println("Manager was already init!");
        }
    }

    @BeforeClass
    public static void start() throws IOException, DevFailed {
        TestDevice server = new TestDevice(
                DEFAULT_NO_DB_DEVICE_NAME,
                JTangoTest.class,
                JTangoTest.SERVER_NAME,
                JTangoTest.INSTANCE_NAME
        );
        
        start(server, server.getDevName());
    }

    protected static void start(TestDevice server, String deviceList) throws IOException, DevFailed {
        init();
        manager.startDevicesForServer(server, deviceList);
    }

    @AfterClass
    public static void stop() throws DevFailed {
        manager.stopDevices();
    }

    public static String getDefaultDeviceFullName() {
        return manager.getDefaultDeviceFullName();
    }

    public static String getFullAdminName() {
        return manager.getFullAdminName();
    }

    public static List<String> getDeviceFullNameList() {
        return manager.getDeviceFullNameList();
    }
}
