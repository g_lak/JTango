package tango.it.runner;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceProxy;
import org.junit.*;
import org.tango.it.ITWithTangoDB;
import org.tango.it.TestDevice;
import org.tango.it.manager.DBDeviceManager;
import org.tango.server.testserver.JTangoTest;

public class ITWithDBRunner {

    private static ITWithTangoDB manager;
    public static Database tangoDatabase;
    private static final String DEFAULT_DEVICE_NAME = "test/tango/jtangotest.1";

    /**
     * boolean which define if we should perform cleaning after every test
     */
    protected static final boolean STOP_TEST_DEVICE_AFTER_EVERY_TEST = false;

    private static void init() throws DevFailed {
        if (manager == null) {
            manager = new DBDeviceManager();
            tangoDatabase = manager.getDatabase();
        }
    }

    @BeforeClass
    public static void setup() throws DevFailed {
        setup(new TestDevice(
                        DEFAULT_DEVICE_NAME,
                        JTangoTest.class,
                        JTangoTest.SERVER_NAME,
                        JTangoTest.INSTANCE_NAME
                )
        );
    }

    protected static void setup(TestDevice device) throws DevFailed {
        System.out.println("Tango host = " + System.getProperty("TANGO_HOST"));
        Assert.assertNotNull(System.getProperty("TANGO_HOST"));
        Assert.assertFalse(Boolean.parseBoolean(System.getProperty("org.tango.server.checkalarms")));

        init();

        manager.addDevice(device);
        manager.startTestDevice();
    }

    protected static String getTestDeviceName() {
        return manager.getRunningDeviceName();
    }

    protected static boolean restartTestDevice() {
        try {
            manager.restartTestDevice();
            return true;
        } catch (Exception e) {
            System.out.println("Can't restart device, exception is throw " + e);
            return false;
        }
    }

    protected DeviceProxy getDeviceProxy() throws DevFailed {
        return new DeviceProxy(getTestDeviceName());
    }
    
    @Before
    public void startDevice() throws DevFailed {
        manager.startTestDevice();
    }

    @AfterClass
    public static void cleanup() throws DevFailed {
        if (!STOP_TEST_DEVICE_AFTER_EVERY_TEST) {
            manager.stopTestDevice();
        }
        manager.removeDevice();
    }

    @After
    public void stopDevice() throws DevFailed {
        if (STOP_TEST_DEVICE_AFTER_EVERY_TEST) {
            manager.stopTestDevice();
        }
    }
}
