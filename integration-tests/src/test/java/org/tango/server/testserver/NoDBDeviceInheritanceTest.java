/**
 * Copyright (C) :     2012
 *
 * 	Synchrotron Soleil
 * 	L'Orme des merisiers
 * 	Saint Aubin
 * 	BP48
 * 	91192 GIF-SUR-YVETTE CEDEX
 *
 * This file is part of Tango.
 *
 * Tango is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tango is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Tango.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.TangoCommand;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.tango.it.ITWithoutTangoDB;
import org.tango.it.TestDevice;
import tango.it.runner.ITWithoutDBRunner;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.tango.server.testserver.SubServer.NO_DB_DEVICE_NAME;

@Category(ITWithoutTangoDB.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NoDBDeviceInheritanceTest extends ITWithoutDBRunner {

    static String deviceName;
    
    @BeforeClass
    public static void start() throws IOException, DevFailed {
        TestDevice server = new TestDevice(
                NO_DB_DEVICE_NAME,
                SubServer.class,
                SubServer.SERVER_NAME,
                SubServer.INSTANCE_NAME
        );

        start(server, server.getDevName());
        deviceName = getDeviceFullNameList().get(0);
    }

    @Test()
    public void test1InitOK() throws DevFailed {
        final TangoAttribute attr = new TangoAttribute(deviceName + "/State");
        assertEquals("ON", attr.read(String.class));
    }

    @Test()
    public void testSetState() throws DevFailed {
        final TangoCommand cmd = new TangoCommand(deviceName, "setDisable");
        cmd.execute();
        final TangoAttribute attr = new TangoAttribute(deviceName + "/State");
        assertEquals("DISABLE", attr.read(String.class));
    }

    @Test
    public void testAttribute() throws DevFailed {
        final TangoAttribute attr = new TangoAttribute(deviceName + "/superAttribute");
        final String writeValue = "hello";
        attr.write(writeValue);
        assertEquals(writeValue, attr.read(String.class));
    }

    @Test
    public void testProperty() throws DevFailed {
        final TangoCommand cmd = new TangoCommand(deviceName, "getSuperDeviceProperty");
        final String prop = cmd.execute(String.class);
        assertEquals("defaultValue", prop);
    }

    @Test
    public void testCommand() throws DevFailed {
        final TangoCommand cmd = new TangoCommand(deviceName, "superCommand");
        final double setPoint = 3.4;
        final double value = cmd.execute(double.class, setPoint);
        assertEquals(setPoint, value, 0.1);
        final TangoAttribute attr = new TangoAttribute(deviceName + "/State");
        assertEquals("EXTRACT", attr.read(String.class));
    }

}
