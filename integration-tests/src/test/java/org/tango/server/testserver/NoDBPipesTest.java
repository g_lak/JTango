/**
 * Copyright (C) :     2012
 *
 * 	Synchrotron Soleil
 * 	L'Orme des merisiers
 * 	Saint Aubin
 * 	BP48
 * 	91192 GIF-SUR-YVETTE CEDEX
 *
 * This file is part of Tango.
 *
 * Tango is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tango is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Tango.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.Tango.DispLevel;
import fr.esrf.Tango.PipeWriteType;
import fr.esrf.TangoApi.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithoutTangoDB;
import org.tango.it.TestDevice;
import org.tango.utils.DevFailedUtils;
import tango.it.runner.ITWithoutDBRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

@Category(ITWithoutTangoDB.class)
public class NoDBPipesTest extends ITWithoutDBRunner {

    @BeforeClass
    public static void start() throws DevFailed, IOException {
        TestDevice server = new TestDevice(
                PipeServer.NO_DB_DEVICE_NAME,
                PipeServer.class,
                PipeServer.SERVER_NAME,
                PipeServer.INSTANCE_NAME
        );

        start(server, server.getDevName());
    }

    @Test
    public void readPipeConfig() throws DevFailed {
        try {

            final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
            final List<PipeInfo> infoList = dev.getPipeConfig();
            final PipeInfo info1 = infoList.get(0);

            assertEquals("coucou", info1.getLabel());
            assertEquals("myPipe", info1.getName());
            assertEquals("hello", info1.getDescription());
            assertEquals(PipeWriteType.PIPE_READ_WRITE, info1.getWriteType());
            assertEquals(DispLevel.EXPERT, info1.getLevel());

            final PipeInfo info2 = infoList.get(1);

            assertEquals("myPipeRO", info2.getName());
            assertEquals("myPipeRO", info2.getLabel());
            assertEquals("No description", info2.getDescription());
            assertEquals(PipeWriteType.PIPE_READ, info2.getWriteType());
            assertEquals(DispLevel.OPERATOR, info2.getLevel());

            /*
            PipeInfo info = infoList.get(3);
            info.setDescription("has been set by java Api");
            deviceProxy.setPipeConfig(infoList);
            */
        } catch (final DevFailed e) {
            e.printStackTrace();
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void writePipeConfig() throws DevFailed {
        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        final List<PipeInfo> infoList = dev.getPipeConfig();
        final PipeInfo info1 = infoList.get(0);
        info1.setDescription("description");
        dev.setPipeConfig(infoList);
        final List<PipeInfo> infoListRead = dev.getPipeConfig();
        final PipeInfo infoRead = infoListRead.get(0);

        assertEquals("description", infoRead.getDescription());
    }

    @Test
    public void readPipe() throws DevFailed {
        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        final DevicePipe pipe = dev.readPipe("myPipe");

        assertEquals("myPipe", pipe.getPipeName());
        assertEquals(1, pipe.getPipeBlob().getDataElementNumber());
        assertEquals("Gif", pipe.nextString());
//        System.out.println(pipe.getPipeName());
//        System.out.println(ReflectionToStringBuilder.toString(pipe.getDevPipeDataObject(),
//                ToStringStyle.MULTI_LINE_STYLE));
//        System.out.println(ReflectionToStringBuilder.toString(pipe.getDevPipeDataObject().data_blob,
//                ToStringStyle.MULTI_LINE_STYLE));
//        System.out.println(ReflectionToStringBuilder.toString(pipe.getPipeBlob(), ToStringStyle.MULTI_LINE_STYLE));
    }

    @Test
    public void writePipe() throws DevFailed {
        try {
            // Build an inner blob
            final PipeBlob children = new PipeBlob("Name / Age");
            children.add(new PipeDataElement("Chloe", 30));
            children.add(new PipeDataElement("Nicolas", 28));
            children.add(new PipeDataElement("Auxane", 21));

            // Build the main blob and insert inner one
            final PipeBlob pipeBlob = new PipeBlob("Pascal");
            pipeBlob.add(new PipeDataElement("City", "Grenoble"));
            pipeBlob.add(new PipeDataElement("Values", new float[] { 1.23f, 4.56f, 7.89f }));
            pipeBlob.add(new PipeDataElement("Children", children));
            pipeBlob.add(new PipeDataElement("Status", DevState.RUNNING));

            final DevicePipe devicePipe = new DevicePipe("myPipe", pipeBlob);
            final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());

            // Then write the pipe
            dev.writePipe(devicePipe);

            // read back for assert
            final DevicePipe pipe = dev.readPipe("myPipe");

            assertEquals("myPipe", pipe.getPipeName());
            assertEquals(4, pipe.getPipeBlob().getDataElementNumber());
            assertEquals("Grenoble", pipe.nextString());

            final float[] array = new float[3];
            pipe.nextArray(array, 3);

            assertArrayEquals(new float[]{1.23f, 4.56f, 7.89f}, array, 0.1F);

            final PipeScanner scanner = pipe.nextScanner();

            assertEquals(30, scanner.nextInt());
            assertEquals(28, scanner.nextInt());
            assertEquals(21, scanner.nextInt());

            // init for others tests
            dev.command_inout("Init");
        } catch (final DevFailed e) {
            e.printStackTrace();
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

}
