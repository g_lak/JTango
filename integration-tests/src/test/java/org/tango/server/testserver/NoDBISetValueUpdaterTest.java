package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithoutTangoDB;
import tango.it.runner.ITWithoutDBRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


@Category(ITWithoutTangoDB.class)
public class NoDBISetValueUpdaterTest extends ITWithoutDBRunner {

    @Test
    public void test() throws DevFailed {
        final TangoAttribute ta = new TangoAttribute(getDefaultDeviceFullName() + "/doubleDynamic");
        final double written = (Double) ta.readWritten();
        assertThat(written, equalTo(120.3));
    }

}
