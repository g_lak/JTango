package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfoEx;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.client.database.DatabaseFactory;
import org.tango.client.database.ITangoDB;
import org.tango.it.ITWithoutTangoDB;
import org.tango.utils.DevFailedUtils;
import tango.it.runner.ITWithoutDBRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


@Category(ITWithoutTangoDB.class)
public class NoDBPropertiesTest extends ITWithoutDBRunner {

    @Test
    public void testFileProperty() throws DevFailed {

        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        final DeviceData devda = dev.command_inout("getMyProperty");

        final String value = devda.extractString();
        Assert.assertEquals("default", value);
    }

    @Test
    public void testProperty() throws DevFailed {
        final ITangoDB db = DatabaseFactory.getDatabase();

        final double d = Math.random();
        final String propValue = Double.toString(d);

        final String propName = "myProp";

        Map<String, String[]> map = new HashMap<>();
        map.put(propName, new String[]{propValue});
        db.setDeviceProperties(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, map);

        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        dev.command_inout("Init");
        DeviceData devda = dev.command_inout("getMyProperty");

        String value = devda.extractString();

        Assert.assertEquals(propValue, value);

        map = new HashMap<>();
        map.put(propName, new String[]{""});
        db.setDeviceProperties(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, map);

        dev.command_inout("Init");
        devda = dev.command_inout("getMyProperty");
        value = devda.extractString();
        Assert.assertEquals("default", value);
    }

    @Test
    public void testbooleanProperty() throws DevFailed {
        final ITangoDB db = DatabaseFactory.getDatabase();

        final String propValue = "true";

        final String propName = "booleanProp";

        final Map<String, String[]> map = new HashMap<>();
        map.put(propName, new String[]{propValue});
        db.setDeviceProperties(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, map);

        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        dev.command_inout("Init");
        final DeviceData devda = dev.command_inout("isBooleanProp");

        final boolean value = devda.extractBoolean();

        Assert.assertTrue(value);
    }

    @Test
    public void testbooleanNumProperty() throws DevFailed {
        final ITangoDB db = DatabaseFactory.getDatabase();

        final String propValue = "1";

        final String propName = "booleanProp";

        final Map<String, String[]> map = new HashMap<>();
        map.put(propName, new String[]{propValue});
        db.setDeviceProperties(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, map);

        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        dev.command_inout("Init");
        final DeviceData devda = dev.command_inout("isBooleanProp");

        final boolean value = devda.extractBoolean();

        Assert.assertTrue(value);
    }

    @Test
    public void testClassProperty() throws DevFailed {
        final ITangoDB db = DatabaseFactory.getDatabase();

        final double dd = Math.random();
        final String propClassValue = Double.toString(dd);
        final String propClassName = "myClassProp";

        Map<String, String[]> map = new HashMap<>();
        map.put(propClassName, new String[]{propClassValue});
        db.setClassProperties(JTangoTest.class.getCanonicalName(), map);

        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        dev.command_inout("Init");
        DeviceData devdaClass = dev.command_inout("getMyClassProperty");

        String[] valueClass = devdaClass.extractStringArray();

        Assert.assertArrayEquals(new String[]{propClassValue}, valueClass);

        map = new HashMap<>();
        map.put(propClassName, new String[]{""});
        db.setClassProperties(JTangoTest.class.getCanonicalName(), map);

        dev.command_inout("Init");
        devdaClass = dev.command_inout("getMyClassProperty");
        valueClass = devdaClass.extractStringArray();
        Assert.assertArrayEquals(new String[]{"classDefault"}, valueClass);
    }

    @Test
    public void testEmptyArrayProperty() throws DevFailed {
        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        DeviceData devdaClass = dev.command_inout("getEmptyArrayProperty");
        String[] valueClass = devdaClass.extractStringArray();
        System.out.println("test array " + Arrays.toString(valueClass));
        Assert.assertArrayEquals(new String[0], valueClass);
    }

    @Test
    public void testAttributeProperty() throws DevFailed {
        try {
            final double dd = Math.random();
            final String value = Double.toString(dd);
            final String attrName = "shortScalar";
            final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
            final AttributeInfoEx info = dev.get_attribute_info_ex(attrName);
            info.description = value;
            dev.set_attribute_info(new AttributeInfoEx[]{info});
            final String actualValue = dev.get_attribute_info_ex(attrName).description;
            Assert.assertEquals(value, actualValue);
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testDefaultAttributeProperty() throws DevFailed {
        final String attrName = "longSpectrum";
        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        final AttributeInfoEx info = dev.get_attribute_info_ex(attrName);
        assertThat(info.alarms.min_alarm, equalTo("0"));
        assertThat(info.alarms.max_alarm, equalTo("10"));
        assertThat(info.min_value, equalTo("-100"));
        assertThat(info.max_value, equalTo("1015054014654325L"));
        assertThat(info.alarms.min_warning, equalTo("3"));
        assertThat(info.alarms.max_warning, equalTo("4"));
        assertThat(info.alarms.delta_t, equalTo("10"));
        assertThat(info.alarms.delta_val, equalTo("20"));
        assertThat(info.description, equalTo("test"));
        Assert.assertArrayEquals(info.enum_label, new String[]{"Not specified"});
        // test IDL5 properties
        final AttributeInfoEx info2 = dev.get_attribute_info_ex("enumAttribute");
        Assert.assertArrayEquals(info2.enum_label, new String[]{"VALUE1", "VALUE2"});
    }

}
