package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.DeviceState;
import org.tango.it.ITWithoutTangoDB;
import org.tango.server.ServerManager;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@Category(ITWithoutTangoDB.class)
public class BuildTests {

    private static final String noDbDeviceName = "1/1/1";
    private static final String noDbGiopPort = "12354";
    public static final String deviceName = "tango://localhost:" + noDbGiopPort + "/" + noDbDeviceName + "#dbase=no";
    private static final String noDbInstanceName = "1";

    @Test
    public void testNoInitTest() throws DevFailed {
        startDetachedNoDb(NoInitDevice.class);
        final DeviceProxy dev = new DeviceProxy(deviceName);
        final TangoAttribute ta = new TangoAttribute(deviceName + "/checkProp");
        assertThat(ta.read(String.class), equalTo("default"));
        ta.write("toto");
        assertThat(ta.read(String.class), equalTo("toto"));
        dev.command_inout("Init");
        assertThat(ta.read(String.class), equalTo("default"));

    }

    @Test(expected = DevFailed.class)
    public void testBad1() throws DevFailed {
        startDetachedNoDb(BadServer.class);
    }

    @Test(expected = DevFailed.class)
    public void testBad2() throws DevFailed {
        startDetachedNoDb(BadServer2.class);
    }

    @Test(expected = DevFailed.class)
    public void testBad3() throws DevFailed {
        startDetachedNoDb(BadServer3.class);
    }

    @Test(expected = DevFailed.class)
    public void testBad4() throws DevFailed {
        startDetachedNoDb(BadServer4.class);
    }

    @Test(expected = DevFailed.class)
    public void testBad5() throws DevFailed {
        startDetachedNoDb(BadServer5.class);
    }

    @Test(timeout = 5000)
    public void testInit() throws DevFailed {
        startDetachedNoDb(InitErrorServer.class);
        final DeviceProxy dev = new DeviceProxy(deviceName);
        assertThat(dev.read_attribute("State").extractDevState(), equalTo(DevState.INIT));
        assertThat(dev.read_attribute("Status").extractString(), containsString("Init in progress"));
        try {
            dev.read_attribute("doubleScalar");
            Assert.fail("CONCURRENT_ERROR must thrown here");
        } catch (DevFailed e) {
        }
        while (dev.state().equals(DevState.INIT)) {
            try {
                Thread.sleep(100);
            } catch (final InterruptedException e) {
            }
        }
        assertThat(dev.state(), equalTo(DevState.FAULT));
        assertThat(dev.status(), containsString("fake error"));
        dev.command_inout("Init");
    }

    @Test
    public void testDefaultState() throws DevFailed {
        startDetachedNoDb(TestDefaultStateServer.class);
        final DeviceProxy dev = new DeviceProxy(deviceName);
        assertThat(dev.state(), equalTo(DevState.UNKNOWN));
        assertThat(dev.status(), equalTo("The device is in UNKNOWN state."));
    }

    @Test
    public void testState() throws DevFailed {
        startDetachedNoDb(TestStateServer.class);
        final DeviceProxy dev = new DeviceProxy(deviceName);
        System.out.println(DeviceState.toString(dev.state()));
        assertThat(dev.state(), equalTo(DevState.ON));
    }

    @After
    public void after() throws DevFailed {
        ServerManager.getInstance().stop();
    }

    private void startDetachedNoDb(final Class<?> deviceClass) throws DevFailed {
        System.setProperty("OAPort", noDbGiopPort);
        ServerManager.getInstance().addClass(deviceClass.getCanonicalName(), deviceClass);
        ServerManager.getInstance().startError(new String[]{noDbInstanceName, "-nodb", "-dlist", noDbDeviceName},
                deviceClass.getCanonicalName());
        // adminName = "tango://localhost:" + noDbGiopPort + "/" +
        // Constants.ADMIN_DEVICE_DOMAIN + "/"
        // + ServerManager.getInstance().getServerName() + "#dbase=no";
    }

}
