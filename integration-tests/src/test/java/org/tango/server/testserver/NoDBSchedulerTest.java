package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.client.database.DatabaseFactory;
import org.tango.it.ITWithoutTangoDB;
import tango.it.runner.ITWithoutDBRunner;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


@Category(ITWithoutTangoDB.class)
public class NoDBSchedulerTest extends ITWithoutDBRunner {

    @Test
    public void test() throws DevFailed {
        final Map<String, String[]> properties = new HashMap<>();
        properties.put("isRunRefresh", new String[]{"true"});
        DatabaseFactory.getDatabase().setDeviceProperties(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, properties);
        new DeviceProxy(getDefaultDeviceFullName()).command_inout("Init");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        final TangoAttribute attr = new TangoAttribute(getDefaultDeviceFullName() + "/isScheduleRunning");
        assertThat(attr.read(boolean.class), equalTo(true));
        properties.put("isRunRefresh", new String[]{"false"});
        DatabaseFactory.getDatabase().setDeviceProperties(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, properties);
        new DeviceProxy(getDefaultDeviceFullName()).command_inout("Init");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        assertThat(attr.read(boolean.class), equalTo(false));
    }

}
