package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithoutTangoDB;
import org.tango.utils.DevFailedUtils;
import tango.it.runner.ITWithoutDBRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


@Category(ITWithoutTangoDB.class)
public class NoDBWriteReadTest extends ITWithoutDBRunner {

    @Test
    public void writeRead() throws DevFailed {
        try {
            final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
            final DeviceAttribute da = dev.read_attribute("doubleScalar");
            final double input = 10.4;
            da.insert(input);
            final DeviceAttribute[] result = dev.write_read_attribute(new DeviceAttribute[] { da },
                    new String[] { "doubleScalar" });
            assertThat(result.length, equalTo(1));
            final double val = result[0].extractDouble();
            assertThat(val, equalTo(input));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

}
