package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithoutTangoDB;
import org.tango.orb.ORBManager;
import org.tango.server.ServerManager;

import java.io.IOException;
import java.net.ServerSocket;

@Category(ITWithoutTangoDB.class)
public class AlreadyRunningTest {

    // XXX: server must be declared in tango db before running this test

    @Test(expected = DevFailed.class)
    public void testDB() throws DevFailed {
        // assertThat(System.getProperty("TANGO_HOST"), notNullValue());
        JTangoTest.start();
        ORBManager.init(true, "dserver/" + JTangoTest.SERVER_NAME + "/" + JTangoTest.INSTANCE_NAME);
    }

    @Test(expected = DevFailed.class)
    public void test() throws DevFailed, IOException {
        try (ServerSocket ss1 = new ServerSocket(0)) {
            ss1.setReuseAddress(true);
            ss1.close();
            JTangoTest.startNoDb(ss1.getLocalPort());
        }
        try (ServerSocket ss2 = new ServerSocket(0)) {
            ss2.setReuseAddress(true);
            ss2.close();
            JTangoTest.startNoDb(ss2.getLocalPort());
        }

    }

    @After
    public void stopDevice() throws DevFailed {
        ServerManager.getInstance().stop();
    }

}
