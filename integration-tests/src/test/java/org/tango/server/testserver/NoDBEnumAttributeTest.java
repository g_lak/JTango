package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfoEx;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithoutTangoDB;
import tango.it.runner.ITWithoutDBRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


@Category(ITWithoutTangoDB.class)
public class NoDBEnumAttributeTest extends ITWithoutDBRunner {

    @Test
    public void scalarSimpleTest() throws DevFailed {
        final TangoAttribute att = new TangoAttribute(getDefaultDeviceFullName() + "/enumAttribute");
        // test default value
        att.read();
        // echo test
        final short value = 1;
        att.write(value);
        final short result = att.read(short.class);
        assertThat(result, equalTo(value));
    }

    @Test(expected = DevFailed.class)
    public void scalarOutOfRangeTest() throws DevFailed {
        final TangoAttribute att = new TangoAttribute(getDefaultDeviceFullName() + "/enumAttribute");
        // test default value
        att.read();
        // echo test
        final short value = 2;
        att.write(value);
    }

    @Test(expected = DevFailed.class)
    public void scalarSetEnumLabelTest() throws DevFailed {
        final TangoAttribute att = new TangoAttribute(getDefaultDeviceFullName() + "/enumAttribute");
        // modify enum list
        final AttributeInfoEx props = att.getAttributeProxy().get_info_ex();
        props.enum_label = new String[] { "ab", "cd", "ef" };
        att.getAttributeProxy().set_info(new AttributeInfoEx[] { props });
    }

    @Test
    public void dynamicScalarSimpleTest() throws DevFailed {
        final TangoAttribute att = new TangoAttribute(getDefaultDeviceFullName() + "/DevEnumDynamic");
        // test default value
        att.read();
        // echo test
        final short value = 1;
        att.write(value);
        final short result = att.read(short.class);
        assertThat(result, equalTo(value));
    }

    @Test
    public void dynamicScalarChangeEnumTest() throws DevFailed {
        final TangoAttribute att = new TangoAttribute(getDefaultDeviceFullName() + "/DevEnumDynamic");

        // test default value
        att.read();
        // modify enum list
        final AttributeInfoEx props = att.getAttributeProxy().get_info_ex();
        props.enum_label = new String[] { "a", "b", "c", "d" };
        att.getAttributeProxy().set_info(new AttributeInfoEx[] { props });
        // write
        final short value = 3;
        att.write(value);
        final short result = att.read(short.class);
        assertThat(result, equalTo(value));
    }

    @Test
    public void dynamicScalarChangeEnumTest2() throws DevFailed {
        final TangoAttribute att = new TangoAttribute(getDefaultDeviceFullName() + "/DevEnumDynamic");
        // test default value
        att.read();
        // modify enum list
        final AttributeInfoEx props = att.getAttributeProxy().get_info_ex();
        props.enum_label = new String[] { "a" };
        att.getAttributeProxy().set_info(new AttributeInfoEx[] { props });
        // write
        final short value = 0;
        att.write(value);
        final short result = att.read(short.class);
        assertThat(result, equalTo(value));
    }

    @Test(expected = DevFailed.class)
    public void dynamicScalarErrorTest() throws DevFailed {
        final TangoAttribute att = new TangoAttribute(getDefaultDeviceFullName() + "/DevEnumDynamic");
        // test default value
        att.read();
        // modify enum list
        final AttributeInfoEx props = att.getAttributeProxy().get_info_ex();
        props.enum_label = new String[] { "a" };
        att.getAttributeProxy().set_info(new AttributeInfoEx[] { props });
        // write
        final short value = 1;
        att.write(value);

    }

}
